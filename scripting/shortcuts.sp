#include <sourcemod>
#include <regex>

#pragma semicolon 1

#define PLUGIN_VERSION				"1.0.0"

public Plugin:myinfo = 
{
	name = "Shortcuts",
	author = "Mitch.",
	description = "Provides chat-triggered shortcuts",
	version = PLUGIN_VERSION,
	url = "http://snbx.info/"
};

new Handle:g_Shortcuts;
new Handle:g_Titles;
new Handle:g_Links;
new Handle:g_LinksNS;
new Handle:g_Flag;

new String:g_ServerIp [32];
new String:g_ServerPort [16];

public OnPluginStart()
{
	CreateConVar( "sm_shortcuts_version", PLUGIN_VERSION, "", FCVAR_NOTIFY|FCVAR_REPLICATED );
	
	LoadTranslations("common.phrases");
	
	g_Shortcuts = CreateArray( 32 );
	g_Titles = CreateArray( 64 );
	g_Links = CreateArray( 512 );
	g_LinksNS = CreateArray( 512 );
	g_Flag = CreateArray( );
	
	new Handle:cvar = FindConVar( "hostip" );
	new hostip = GetConVarInt( cvar );
	FormatEx( g_ServerIp, sizeof(g_ServerIp), "%u.%u.%u.%u",
		(hostip >> 24) & 0x000000FF, (hostip >> 16) & 0x000000FF, (hostip >> 8) & 0x000000FF, hostip & 0x000000FF );
	
	cvar = FindConVar( "hostport" );
	GetConVarString( cvar, g_ServerPort, sizeof(g_ServerPort) );
	
	LoadWebshortcuts();
}
 
public Action:Command_Callback( client, args )
{
	decl String:sCommand[50];
	GetCmdArg(0,sCommand, sizeof(sCommand));
	new i;
	if((i=FindStringInArray(g_Shortcuts, sCommand))!= -1) //Should never return -1!
	{
		new String:sBuffer[8];
		GetArrayString( g_Titles, i, sBuffer, sizeof(sBuffer) );
		new Flag = GetArrayCell( g_Flag, i);
		if(StrEqual(sBuffer, "P", false))
		{
			decl String:sArg1[50];
			GetCmdArg(1,sArg1, sizeof(sArg1));
			new target;
			if((target = FindTargetEx(client, sArg1, true, false, false))!= -1)
			{
				if(Flag!=0 && target != client)
				{
					if(DoesUserHaveFlag(client, Flag) || DoesUserHaveFlag(client, ADMFLAG_ROOT) || DoesUserHaveFlag(target, Flag) || DoesUserHaveFlag(target, ADMFLAG_ROOT))
					{
						IssueCmd(client, target, i);
					}
				}
				else IssueCmd(client, target, i);
			}
			else DisplayClientMenu(client, i, Flag);
		}
		else IssueCmd(client, 0, i);
	}
	return Plugin_Handled;	
}
IssueCmd(iClient, iTarget, iCmdIndex)
{
	new String:text [512];
	new String:steamId [64];
	new String:steamId64 [64];
	new String:userId [16];
	new String:name [64];
	new String:clientIp [32];
	
	if(iTarget>0)
	{
		GetArrayString( g_Links, iCmdIndex, text, sizeof(text) );
		GetClientAuthString( iTarget, steamId, sizeof(steamId) );
		FormatEx( userId, sizeof(userId), "%u", GetClientUserId( iTarget ) );
		GetClientName( iTarget, name, sizeof(name) );
		GetClientIP( iTarget, clientIp, sizeof(clientIp) );
		SteamIDToCommunityID(steamId,steamId64,sizeof(steamId64));
	}
	else GetArrayString( g_LinksNS, iCmdIndex, text, sizeof(text) );
	
	ReplaceString( text, sizeof(text), "{SERVER_IP}", g_ServerIp);
	ReplaceString( text, sizeof(text), "{SERVER_PORT}", g_ServerPort);
	ReplaceString( text, sizeof(text), "{STEAM_ID}", steamId);
	ReplaceString( text, sizeof(text), "{STEAM_ID64}", steamId64);
	ReplaceString( text, sizeof(text), "{USER_ID}", userId);
	ReplaceString( text, sizeof(text), "{NAME}", name);
	ReplaceString( text, sizeof(text), "{IP}", clientIp);
	
	DisplayMOTDWithOptions(iClient, "MOTD WINDOW", text, true, true, MOTDPANEL_TYPE_URL);
}
stock ReplaceSlashes(String:buffer[], bool:removeTags=false, maxlen=512) {
	new cursor = 0;
	decl String:tag[32], String:output[maxlen];
	strcopy(output, maxlen, buffer);
	// Since the string's size is going to be changing, output will hold the replaced string and we'll search buffer
	
	new Handle:regex = CompileRegex("\\(.+\\)");
	for(new i = 0; i < 1000; i++) { // The RegEx extension is quite flaky, so we have to loop here :/. This loop is supposed to be infinite and broken by return, but conditions have been added to be safe.
		if(MatchRegex(regex, buffer[cursor]) < 1) {
			CloseHandle(regex);
			strcopy(buffer, maxlen, output);
			return;
		}
		GetRegexSubString(regex, 0, tag, sizeof(tag));
		cursor = StrContains(buffer[cursor], tag, false) + cursor + 1;
		if(removeTags) {
			ReplaceString(output, maxlen, tag, "", false);
		}
	}
	LogError("[SHORTCUTS] Infinite loop broken.");
}

DisplayClientMenu(client, iCmdIndex = 0, iFlag = 0) {
	new Handle:menu = CreateMenu(Handler_ClientMenu);
	decl String:sTitle[256];
	GetArrayString( g_Shortcuts, iCmdIndex, sTitle, sizeof(sTitle) );
	Format(sTitle, sizeof(sTitle), "%c%s:\nSelect Player:",CharToUpper(sTitle[0]),sTitle[1]);
	SetMenuTitle(menu, sTitle);
	decl String:name[MAX_NAME_LENGTH];
	decl String:iOpt[32];
	//Main Site
	Format(iOpt, sizeof(iOpt), "%i:-1",iCmdIndex);
	AddMenuItem(menu, iOpt, "Goto Site");
	//Self
	Format(iOpt, sizeof(iOpt), "%i:%d",iCmdIndex,GetClientUserId(client));
	AddMenuItem(menu, iOpt, "Self");
	//CheckCommandAccess(client, "", ReadFlagString(const String:flags[]), true);
	new bool:HasFlag = (DoesUserHaveFlag(client, iFlag) || DoesUserHaveFlag(client, ADMFLAG_ROOT) );
	for(new i = 1; i <= MaxClients; i++) {
		if(!IsClientInGame(i) || IsFakeClient(i) || i == client) {
			continue;
		}
		if(iFlag!=0)
		{
			if(HasFlag)
			{
				GetClientName(i, name, sizeof(name));
				Format(iOpt, sizeof(iOpt), "%i:%d",iCmdIndex,GetClientUserId(i));
				AddMenuItem(menu, iOpt, name);
			}
			else
			{
				if(DoesUserHaveFlag(i, iFlag) || DoesUserHaveFlag(i, ADMFLAG_ROOT) )
				{
					GetClientName(i, name, sizeof(name));
					Format(iOpt, sizeof(iOpt), "%i:%d",iCmdIndex,GetClientUserId(i));
					AddMenuItem(menu, iOpt, name);
				}
			}
		}
		else
		{
			GetClientName(i, name, sizeof(name));
			GetArrayString( g_Shortcuts, iCmdIndex, sTitle, sizeof(sTitle) );
			Format(iOpt, sizeof(iOpt), "%i:%d",iCmdIndex,GetClientUserId(i));
			AddMenuItem(menu, iOpt, name);
		}
	}
	DisplayMenu(menu, client, 0);
}

public Handler_ClientMenu(Handle:menu, MenuAction:action, client, param) {
	if(action == MenuAction_End) {
		CloseHandle(menu);
	}
	if(action != MenuAction_Select) {
		return;
	}
	decl String:selection[32];
	GetMenuItem(menu, param, selection, sizeof(selection));
	new String:sBuffer[2][32];
	ExplodeString(selection, ":", sBuffer, 2, 32);
	IssueCmd(client, GetClientOfUserId(StringToInt(sBuffer[1])), StringToInt(sBuffer[0]));
}
 
LoadWebshortcuts()
{
	decl String:buffer [1024];
	BuildPath( Path_SM, buffer, sizeof(buffer), "configs/shortcuts.txt" );
	
	if ( !FileExists( buffer ) )
	{
		return;
	}
 
	new Handle:f = OpenFile( buffer, "r" );
	if ( f == INVALID_HANDLE )
	{
		LogError( "[SM] Could not open file: %s", buffer );
		return;
	}
	
	ClearArray( g_Shortcuts );
	ClearArray( g_Titles );
	ClearArray( g_Links );
	ClearArray( g_LinksNS );
	ClearArray( g_Flag );
	
	
	decl String:shortcut [32];
	decl String:title [64];
	decl String:link [512];
	decl String:link2 [512];
	while ( !IsEndOfFile( f ) && ReadFileLine( f, buffer, sizeof(buffer) ) )
	{
		TrimString( buffer );
		if ( buffer[0] == '\0' || buffer[0] == ';' || ( buffer[0] == '/' && buffer[1] == '/' ) )
		{
			continue;
		}
		
		new pos = BreakString( buffer, shortcut, sizeof(shortcut) );
		if ( pos == -1 )
		{
			continue;
		}
		
		new linkPos = BreakString( buffer[pos], title, sizeof(title) );
		if ( linkPos == -1 )
		{
			continue;
		}
		if(StrContains(title, ":")>=0)
		{
			new String:sBuffer[2][32];
			ExplodeString(title, ":", sBuffer, 2, 32);
			PushArrayString( g_Titles, sBuffer[0] );
			PushArrayCell( g_Flag, ReadFlagString(sBuffer[1]) );
		}
		else
		{
			PushArrayString( g_Titles, title );
			PushArrayCell( g_Flag, 0 );
		}
		
		
		
		strcopy( link, sizeof(link), buffer[linkPos+pos] );
		strcopy( link2, sizeof(link2), buffer[linkPos+pos] );
		TrimString( link );
		TrimString( link2 );
		RegConsoleCmd(shortcut, Command_Callback, "Quick-Link");
		PushArrayString( g_Shortcuts, shortcut );
		ReplaceString(link, 512, "(", "", false);
		ReplaceString(link, 512, ")", "", false);
		PushArrayString( g_Links, link );
		
		ReplaceSlashes(link2, true, 512);
		PushArrayString( g_LinksNS, link2 );
	}
	
	CloseHandle( f );
}
stock SteamIDToCommunityID(const String:s[],String:c[],l)
{
	decl String:b[3][32];
	ExplodeString(s,":",b,sizeof(b),sizeof(b[]));
	new a=StringToInt(b[2])*2+StringToInt(b[1]);
	IntToString((a+60265728),c,l);
	if(a>=39734272)
	{
		strcopy(c,l,c[1]);
		Format(c,l,"765611980%s",c);
	}
	else
	{
		Format(c,l,"765611979%s",c);
	}
}

public DisplayMOTDWithOptions(iClient, const String:sTitle[], const String:sUrl[], bool:bBig, bool:bNotSilent, iType)
{
	new Handle:hKv = CreateKeyValues("motd");

	if (bBig)
	{
		KvSetNum(hKv, "customsvr", 1);
	}
	
	KvSetNum(hKv, "type", iType);
	
	if (sTitle[0] != '\0')
	{
		KvSetString(hKv, "title", sTitle);
	}
		
	if (sUrl[0] != '\0')
	{
		KvSetString(hKv, "msg", sUrl);
	}
	
	ShowVGUIPanel(iClient, "info", hKv, bNotSilent);
	CloseHandle(hKv);
}
FindTargetEx(client, const String:target[], bool:nobots = false, bool:immunity = true, bool:replyToError = true) {
	decl String:target_name[MAX_TARGET_LENGTH];
	decl target_list[1], target_count, bool:tn_is_ml;
	
	new flags = COMMAND_FILTER_NO_MULTI;
	if(nobots) {
		flags |= COMMAND_FILTER_NO_BOTS;
	}
	if(!immunity) {
		flags |= COMMAND_FILTER_NO_IMMUNITY;
	}
	
	if((target_count = ProcessTargetString(
			target,
			client, 
			target_list, 
			1, 
			flags,
			target_name,
			sizeof(target_name),
			tn_is_ml)) > 0)
	{
		return target_list[0];
	} else {
		if(replyToError) {
			ReplyToTargetError(client, target_count);
		}
		return -1;
	}
}

stock bool:DoesUserHaveFlag(iClient, iFlag)
{
	new AdminId:iAdmin = GetUserAdmin(iClient);
	if(iAdmin != INVALID_ADMIN_ID)
	{
		if(GetUserFlagBits(iClient) & iFlag)
			return true;
	}
	return false;
}